﻿using LapisInvalidStrings.Data.Models.Mongo;
using LapisInvalidStrings.Domain;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LapisInvalidStrings.Data.Repos
{
    internal class MongoRepo : IMongoRepo
    {
        private readonly MongoClient _dbClient;
        private readonly IMongoDatabase _db;

        public MongoRepo()
        {
            _dbClient = new MongoClient("mongodb://localhost:27017"); //TODO: Fix hardcoded connection string
            _db = _dbClient.GetDatabase("invalidStrings");
        }

        public async Task<IEnumerable<TType>> GetDocumentsAsync<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase
        {
            getCollection<TType>(out var collection);

            var document = collection.Find(query);

            if (document is not null)
            {
                return await document.ToListAsync();
            }
            else
            {
                //Return an empty List
                return new List<TType>();
            }
        }

        public async Task<bool> DocumentExists<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase
        {
            getCollection<TType>(out var collection);

            var document = await collection.FindAsync(query);
            return document.FirstOrDefault() is not null;
        }

        public async Task<IEnumerable<TType>> InsertDocumentsAsync<TType>(IEnumerable<TType> documents)
            where TType : MongoDocumentBase
        {
            getCollection<TType>(out var collection);
            await collection.InsertManyAsync(documents);

            return documents;
        }

        public async Task<DeleteResult> DeleteDocumentsAsync<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase
        {
            getCollection<TType>(out var collection);
            return await collection.DeleteManyAsync(query);
        }

        private void getCollection<TType>(out IMongoCollection<TType> collection)
            where TType : MongoDocumentBase
        {
            collection = _db.GetCollection<TType>(typeof(TType).Name);
        }
    }

    public interface IMongoRepo
    {
        Task<DeleteResult> DeleteDocumentsAsync<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase;
        Task<bool> DocumentExists<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase;
        Task<IEnumerable<TType>> GetDocumentsAsync<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase;
        Task<IEnumerable<TType>> InsertDocumentsAsync<TType>(IEnumerable<TType> documents)
            where TType : MongoDocumentBase;
    }
}
