﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LapisInvalidStrings.Data.Models.Mongo
{
    public class MongoInvalidString : MongoDocumentBase
    {
        [BsonRequired]
        public string InvalidString { get; set; }

        [BsonRequired]
        public string AddedBy { get; set; }

        [BsonRequired]
        public DateTime DateAdded { get; set; }
    }
}
