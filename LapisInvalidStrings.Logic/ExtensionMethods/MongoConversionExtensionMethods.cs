﻿using LapisInvalidStrings.Data.Models.Mongo;
using LapisInvalidStrings.Domain.Models;
using LapisInvalidStrings.Data.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LapisInvalidStrings.Logic
{
    public static class MongoConversionExtensionMethods
    {
        public static InvalidString ToInvalidString(this MongoInvalidString mongoDoc) =>
            new InvalidString()
            {
                Id = mongoDoc.Id,
                Value = mongoDoc.InvalidString,
                AddedBy = mongoDoc.AddedBy,
                DateAdded = mongoDoc.DateAdded
            };
    }
}
