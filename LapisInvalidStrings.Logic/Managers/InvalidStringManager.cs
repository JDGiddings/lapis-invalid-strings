﻿using LapisInvalidStrings.Data.Repos;
using LapisInvalidStrings.Data.Models.Mongo;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LapisInvalidStrings.Domain.Models;
using LapisInvalidStrings.Logic.InsertRequests;

namespace LapisInvalidStrings.Logic.Managers
{
    internal class InvalidStringManager : IInvalidStringManager
    {
        private readonly IMongoRepo _repo;
        private static HashSet<string> _invalidStrings; //Maintain a static hashset of invalid strings in memory for O(1) lookups

        public InvalidStringManager(IMongoRepo repo)
        {
            _repo = repo;

            //Ideally this only runs once, upon startup. Then the database and hashset are kept sync'd from then on
            _invalidStrings = _repo
                                .GetDocumentsAsync<MongoInvalidString>(x => true)
                                .GetAwaiter()
                                .GetResult()
                                .Select(x => x.InvalidString)
                                .ToHashSet();
        }

        public async Task<IEnumerable<InvalidString>> AddInvalidStrings(InvalidStringInsertRequest request)
        {
            //HashSet is fault tolerant. If you try to add a string more than once it will simply ignore it
            request.InvalidStrings.Select(x => _invalidStrings.Add(x));
 
            var mongoDocuments = await _repo.InsertDocumentsAsync(request.ToMongoInvalidStrings());
            return mongoDocuments.Select(x => x.ToInvalidString());
        }

        public async Task<long> DeleteInvalidStrings(IEnumerable<string> invalidStrings)
        {
            invalidStrings.Select(x => _invalidStrings.Remove(x));

            var delresult = await _repo.DeleteDocumentsAsync<MongoInvalidString>(x => invalidStrings.Contains(x.InvalidString));
            return delresult.DeletedCount;
        }

        //Probably doesn't need to be async
        public Task<bool> IsInvalid(string queryString) =>
            Task.Run(() => _invalidStrings.Contains(queryString));

        public async Task<IEnumerable<InvalidString>> GetAllInvalidStrings()
        {
            var documents = await _repo.GetDocumentsAsync<MongoInvalidString>(x => true);
            return documents.Select(x => x.ToInvalidString());
        }
    }

    public interface IInvalidStringManager
    {
        Task<IEnumerable<InvalidString>> AddInvalidStrings(InvalidStringInsertRequest request);
        Task<long> DeleteInvalidStrings(IEnumerable<string> invalidStrings);
        Task<bool> IsInvalid(string queryString);
        Task<IEnumerable<InvalidString>> GetAllInvalidStrings();
    }
}