﻿using LapisInvalidStrings.Logic.Managers;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisInvalidStrings.Logic
{
    public static class Startup
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IInvalidStringManager, InvalidStringManager>();

            Data.Startup.Register(services);
        }
    }
}
